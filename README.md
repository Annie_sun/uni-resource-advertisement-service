## Software Project implemented using Agile Software Development  

### Summary: 
* Service for finding matching students for tutoring, studying and resource sharing
* Written with a combination of different languages: Python, SQLite, JavaScript, AngularJS, HTML and CSS

#### Advertisements include:
* Textbooks (Buy, Sell, Swap)
* Tutoring (Tutor & Tutee)
* Study Buddy
* Events



[ See finished product ](http://cgi.cse.unsw.edu.au/~z5075255/COMP4920/html/#!/main)

